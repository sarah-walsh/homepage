// component containing a grid of project cards that redirect users to other pages

import React from "react";
import { Box, Grid } from "grommet";
import { GRID_AREAS, GRID_COLUMNS, GRID_ROWS } from "./MainPage.styles";

import ProjectCard from "./ProjectCard";

import x_and_o_img from '../noughtsandcrosses.png';
import calc_img from '../calculator.png';
import to_do_img from '../todoapp.png'
import { CALC_URL, TO_DO_URL, X_AND_O_URL } from "../constants/urls";

const MainPage: React.FC = (): React.ReactElement => {
  return (
    <Box
      responsive={true}
      pad="medium"
      justify="center"
      gap="xxsmall"
      alignContent="center"
      align="center"
      alignSelf="center"
    >
      <Grid
        responsive={true}
        rows={GRID_ROWS}
        columns={GRID_COLUMNS}
        areas={GRID_AREAS}
        gap="small"
      >
        <Box
          gridArea="projectone"
          gap="small"
          pad="xsmall"
          direction="row"
          alignContent="center"
          justify="center"
        >
          <ProjectCard
            image={x_and_o_img}
            label="Noughts and Crosses Project"
            url={X_AND_O_URL}
          />
        </Box>
        <Box
          gridArea="projecttwo"
          gap="small"
          pad="xsmall"
          direction="row"
          alignContent="center"
          justify="center"
        >
          <ProjectCard
            image={to_do_img}
            label="To-Do List Project"
            url={TO_DO_URL}
          />
        </Box>
        <Box
          gridArea="projectthree"
          gap="small"
          pad="xsmall"
          direction="row"
          alignContent="center"
          justify="center"
        >
          <ProjectCard
            image={calc_img}
            label="Calculator Project"
            url={CALC_URL}
          />
        </Box>
      </Grid>
    </Box>
  );
};

export default MainPage;
