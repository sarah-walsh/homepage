# homepage

## Overview
This repository contains the frontend and backend code for my homepage application.
The backend is written using Python, while the frontend uses React JS and is written using TypeScript.
<br/>
This application allows users to navigate to other applications by clicking on cards.
Each card displays a label, and an image of the application that the card links to.
<br />
The application is hosted [here](https://www.sarahgwalsh.co.uk/) using AWS Amplify.
